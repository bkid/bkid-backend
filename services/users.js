let Card = require('../models/Cards');

let getUserIdByUID = async (UID) => {
	let userID = ""
	console.log("UID: ", UID)
	await Card.findOne({UID: UID}, (err, card) => {
    if(err) {
      console.error(err)
    } else {
      if(card) {
      	console.log("card: ", card)
        userID = card.userID
      }
    }
  })
  console.log("userID: ",userID)
  return userID
}

module.exports = getUserIdByUID;